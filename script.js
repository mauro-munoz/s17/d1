console.log(`Hello World`);

const students = [`2. Christopher Villegas`,
`3. Roxanne Talampas`,
`4. Dave Supan`,
`5. Amiel Canta`,
`6. Matthew Blasco`,
`7. Chester Clavio`,
`8. Angelito Quiambao`,
`9. Kalvin Philip Rosales`,
`10. Kristian Zantua`,
`11. Craig Brian Guarnino`,
`12. Michael Bermal`,
`13. Kristin Ramos`,
`14. Mauro III Munoz`,
`15. Marc Allen Nanong`,
`16. Gerard Apinado`,
`17. Prince Andwill Monisit`,
`18. Mikhaella Ambrosio`,
`19. Carine Cruz`,
`20. Jeremy Carangan`,
`21. Ian Tablada`,
`22. Mark Allan Gomez`,
`23. Gab Fugaban`,
`24. Kevin Tud`,
`25. Akkram Bederi`];
//how to get number of items in an array
console.log(`Number of students are ${students.length}`)

//how to add new item at the beginning
console.log(students.unshift(`try`));
//how to add a new item at the end
console.log(students.push(`New Student Start`));
//how to remove an item at the beginning
console.log(students.shift());
//how to remove an item at the end
console.log(students.pop());
console.log(students)
//how to find the index number of an element in an array
console.log(students.indexOf(`14. Mauro III Munoz`))
console.log(students[12]);
//if there are two identical existing element in an array, find the first item's index
console.log(students.push(`14. Mauro III Munoz`))
console.log(students);

function firstDuplicateIndex (item) {
	return item == `14. Mauro III Munoz`
}
console.log (students.findIndex(firstDuplicateIndex)); //should return 12
//if there are two identical existing element in an array, find the last item's index

let sampleObj = {
	firstName : `Mauro`,
	lastName: `Munoz`,
	Age: 30,
	Address: {
		Country: `Philippines`,
		City: `Baguio`,
		Barangay: `Upper QM`
	},
	Friends: [`Mark`,`Jay`,`Irah`],
	Admin: true,
	Introduction: function(){
		return `Hi my name is ${this.firstName} my age is ${this.Age}`;
	}
}

// Object Deconstruction
	//firstName: fname is remapping
	//lastName =`Ruiz` is default value, Munoz will still show

let {firstName: fname, lastName = `Ruiz`, ...other} = sampleObj;
console.log(fname);
console.log(lastName);
console.log(other);
let {Country, ...stuff} = sampleObj.Address;
console.log(stuff);




// Array Deconstruction
	// ... is spread can be used like concat
	// rest will be a new array
let [a,b,...rest]=students;
console.log(a);
console.log(b);
console.log(rest);
let [c,d,...rest2] = rest;
console.log(c);
console.log(d);
console.log(rest2);

// Arrow function

function sum(a, b){
	return a + b;
}
	// Converted arrow function
let sum2 = (a , b) => a + b;

function printStuff (guest) {
	console.log(`I am ${guest}`)
}

let printStuff2 = (guest2) => console.log(`I am ${guest2}`);


let printStuff3 = (named, ageyo) => {
	let Obj = {namef: named, age: ageyo};
	let {namef, age} = Obj;
	console.log(`I am ${namef} my age is ${age}`)
}
